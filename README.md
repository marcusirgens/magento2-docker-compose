# magento2-docker-compose

This is a basic docker-compose config that should support Magento2 out of the box.

## Nice to know:
- You can attach to the 7.1-apache service by doing ```docker exec -i -t magento2-docker-compose_apache_1 /bin/bash```. Replace ```magento2-docker-compose``` with the name of the service.
- Put your magento install in ```app/magento```, or change the path in ```docker-compose.yml```
- ```composer``` and ```n98-magerun2``` are globally available (they are symlinked to /usr/bin/)
- You can uncomment some lines in ```docker/httpd/Dockerfile``` to enable SSL, the server will generate a certificate for you
- phpmyadmin is available at localhost:8080

## Services
### Redis
Available at the standard port

### MariaDB
Available at the standard port as ```root```/```root```. Hostname is ```db```, database is ```magento```.

### Logs
Apache logs to ```log```.

## Help!
Cheat sheet:   
```docker-compose up -d --build``` should get you up and running most of the time.   
```docker-compose down --remove-orphans``` should get you down.   
```docker ps -a``` should show your running containers.   
```docker exec -i -t magento2-docker-compose_apache_1 /bin/bash``` gets you inside the php-apache container.   

Feel free to send pull requests.
