#!/bin/sh

if [ ! -f /etc/apache2/cert/server.crt ]; then
    mkdir -p /etc/apache2/cert/
    openssl req -subj '/CN=localhost/O=Trollweb/C=NO' -new -newkey rsa:2048 -sha256 -days 365 -nodes -x509 -keyout /etc/apache2/cert/server.key -out /etc/apache2/cert/server.crt
fi